<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function wishlist()
    {
        return $this->belongsTo('App\Wishlist', 'wishlist_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }
}
