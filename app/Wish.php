<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wish extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id', 'wishlist_id', 'url'
    ];


    public function user() {
        return $this->belongsTo('App\User');
    }

    public function wishlists() {
        return $this->belongsToMany('App\Wishlist', 'wishlist_wishes', 'wish_id', 'wishlist_id');
    }
}
