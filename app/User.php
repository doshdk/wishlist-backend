<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'newsletter', 'picture', 'facebook_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function wishlists() {
        return $this->hasMany('App\Wishlist');
    }

    public function wishes() {
        return $this->hasMany('App\Wish');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function following() {
        return $this->belongsToMany('App\Wishlist', 'wishlist_followers', 'user_id', 'wishlist_id');
    }
}
