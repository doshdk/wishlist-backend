<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateAccountRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;

use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;


class AuthController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    // Facebook validation of access token
    // https://graph.facebook.com/debug_token?%20input_token=EAAB6w6p4Vg8BAKZClDW8eKRvc2lOvQX06jcQddMT2esX7jZCJR7B0w5yzZAeDAH7iJvrdp6ZCtpZCzYKhhdZAWgZBsPQZCvOD4t8GlbUeTJp1Aifl3vTIStcZCs9OiFhTr3nTEwXZCD9ZArrHOr7oLvuSXRxGxk4ZBN1ZBEdkKnaWObOPIldaYNH93ly7dm9bfaOz5aphP6JpOZBAuZBQZDZD&access_token=134980797224463|51i5WchZhXlooSq3I3K9oXAytrk


    /**
     * Authenticated the user with the given credentials and returns a token
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(LoginRequest $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json([
            'token' => $token,
            'user' => Auth::user()
        ]);
    }


}
