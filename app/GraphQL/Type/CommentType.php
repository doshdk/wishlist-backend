<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use App\User;
use App\Wishlist;

class CommentType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Comment',
        'description' => 'A comment'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
// protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the comment'
            ],
            'message' => [
                'type' => Type::string(),
                'description' => 'The message of the comment'
            ],
            'author' => [
                'type' => \GraphQL::type('User'),
                'description' => 'The author of the comment'
            ],
            'wishlist' => [
                'type' => \GraphQL::type('Wishlist'),
                'description' => 'The wishlist that the comment it posted to'
            ],
            'replies' => [
                'type' => Type::listOf(\GraphQL::type('Comment')),
                'description' => 'Replies made to this comment'
            ],
            'parent' => [
                'type' => \GraphQL::type('Comment'),
                'description' => 'The parent comment of the comment'
            ]
        ];
    }

    protected function resolveAuthorField($root, $args)
    {
        return $root->user;
    }

    protected function resolveRepliesField($root, $args)
    {
        return $root->replies;
    }

    protected function resolveParentField($root, $args)
    {
        return $root->parent;
    }

    protected function resolveWishlistField($root, $args)
    {
        return $root->wishlist;
    }

}