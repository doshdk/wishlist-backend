<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use App\User;
use App\wish;

class wishType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Wish',
        'description' => 'A wish'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
// protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the wish'
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of the wish'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the wish'
            ],
            'url' => [
                'type' => Type::string(),
                'description' => 'The url of the wish'
            ],
            'owner' => [
                'type' => \GraphQL::type('User'),
                'description' => 'The owner of the wish'
            ],
            'wishlists' => [
                'type' => Type::listOf(\GraphQL::type('Wishlist')),
                'description' => 'Wishlists this wish is attached to'
            ],
        ];
    }

    protected function resolveOwnerField($root, $args)
    {
        return $root->user;
    }

    protected function resolveWishlistsField($root, $args)
    {
        return $root->wishlists;
    }

}