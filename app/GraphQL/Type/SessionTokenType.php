<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class SessionTokenType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Session',
        'description' => 'The user session'
    ];

    public function fields()
    {
        return [
            'token' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The JWT token of the session'
            ],
            'user' => [
                'type' => \GraphQL::type('User'),
                'description' => 'The user attached to the session'
            ]
        ];
    }

    protected function resolveUserField($root, $args)
    {
        $user = Auth::user();
        return $user;
    }

    protected function resolveTokenField($root, $args)
    {
        return 'Bearer ' . $root;
    }


}