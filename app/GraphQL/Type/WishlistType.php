<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use App\User;
use App\Wishlist;

class WishlistType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Wishlist',
        'description' => 'A wishlist'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
// protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the wishlist'
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of the wishlist'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the wishlist'
            ],
            'cover_image' => [
                'type' => Type::string(),
                'description' => 'The url to the cover image of the wishlist'
            ],
            'owner' => [
                'type' => \GraphQL::type('User'),
                'description' => 'The owner of the wishlist'
            ],
            'followers' => [
                'type' => Type::listOf(\GraphQL::type('User')),
                'description' => 'Users following the wishlist'
            ],
            'comments' => [
                'type' => Type::listOf(\GraphQL::type('Comment')),
                'description' => 'Comments posted to the wishlist'
            ],
            'wishes' => [
                'type' => Type::listOf(\GraphQL::type('Wish')),
                'description' => 'Wishes attached to the wishlist'
            ]
        ];
    }

    protected function resolveOwnerField($root, $args)
    {
        return $root->user;
    }

    protected function resolveFollowersField($root, $args)
    {
        return $root->followers;
    }

    protected function resolveWishesField($root, $args)
    {
        return $root->wishes;
    }

    protected function resolveCommentsField($root, $args)
    {
        return $root->comments;
    }

}