<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'A user'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the user'
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'The email of the user',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the user'
            ],
            'picture' => [
                'type' => Type::string(),
                'description' => 'The profile picture of the user'
            ],
            'newsletter' => [
                'type' => Type::boolean(),
                'description' => 'If the user is signed up for the newsletter'
            ],
            'wishlists' => [
                'type' => Type::listOf(\GraphQL::type('Wishlist')),
                'description' => 'Wishlists created by the user'
            ],
            'following' => [
                'type' => Type::listOf(\GraphQL::type('Wishlist')),
                'description' => 'Wishlists followed by the user'
            ],
            'comments' => [
                'type' => Type::listOf(\GraphQL::type('Comment')),
                'description' => 'Comments posted by the user'
            ]
        ];
    }

// If you want to resolve the field yourself, you can declare a method
// with the following format resolve[FIELD_NAME]Field()
    protected function resolveEmailField($root, $args)
    {
        $user = Auth::user();

        if($user == null) {
            try {
                $user = JWTAuth::parseToken()->authenticate();
            } catch(JWTException $error) {

            }
        }

        if($user->id == $root->id) {
            return strtolower($root->email);
        }

        return null;
    }

    protected function resolvFollowingField($root, $args)
    {
        return strtolower($root->following);
    }

    protected function resolveCommentsField($root, $args)
    {
        return $root->comments;
    }
}