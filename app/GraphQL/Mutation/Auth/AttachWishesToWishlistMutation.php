<?php
namespace App\GraphQL\Mutation\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\Wishlist;
use Illuminate\Support\Facades\DB;

class AttachWishesToWishlistMutation extends Mutation
{
    protected $attributes = [
        'name' => 'attachWishesToWishlist'
    ];

    public function type()
    {
        return GraphQL::type('Wishlist');
    }

    public function args()
    {
        return [
            'wishId' => ['name' => 'wishId', 'type' => Type::listOf(Type::string())],
            'wishlistId' => ['name' => 'wishlistId', 'type' => Type::string()],
        ];
    }

    public function rules()
    {
        return [
            'wishId' => ['required'],
            'wishlistId' => ['required'],
        ];
    }

    public function resolve($root, $args)
    {

        $wishlist = Wishlist::find($args['wishlistId']);

        if (!$wishlist) {
            return null;
        }

        if (isset($args['wishlistId'])) {
            foreach ($args['wishId'] as $wishId) {

                $exists = DB::table('wishlist_wishes')
                        ->whereWishId($wishId)
                        ->whereWishlistId($args['wishlistId'])
                        ->count() > 0;

                if(!$exists) {
                    $wishlist->wishes()->attach($wishId);
                }
            }
        }

        return $wishlist;
    }
}