<?php
namespace App\GraphQL\Mutation\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\Wishlist;
use Tymon\JWTAuth\Facades\JWTAuth;

class CreateWishlistMutation extends Mutation
{
    protected $attributes = [
        'name' => 'createWishlist'
    ];

    public function type()
    {
        return GraphQL::type('Wishlist');
    }

    public function args()
    {
        return [
            'title' => ['name' => 'title', 'type' => Type::nonNull(Type::string())],
            'description' => ['name' => 'description', 'type' => Type::string()],
            'cover_image' => ['name' => 'cover_image', 'type' => Type::string()],
        ];
    }

    public function rules()
    {
        return [
            'title' => ['required'],
        ];
    }

    public function resolve($root, $args)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if($user === null) {
            return null;
        } else {
            $args['user_id'] = $user->id;
        }

        $wishlist = Wishlist::create($args);

        if (!$wishlist) {
            return null;
        }

        return $wishlist;
    }
}