<?php
namespace App\GraphQL\Mutation\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\Wishlist;
use App\Wish;

class CreateWishMutation extends Mutation
{
    protected $attributes = [
        'name' => 'createWish'
    ];

    public function type()
    {
        return GraphQL::type('Wish');
    }

    public function args()
    {
        return [
            'userId' => ['name' => 'userId', 'type' => Type::nonNull(Type::string())],
            'title' => ['name' => 'title', 'type' => Type::nonNull(Type::string())],
            'wishlistId' => ['name' => 'wishlistId', 'type' => Type::string()],
            'description' => ['name' => 'description', 'type' => Type::string()],
            'url' => ['name' => 'url', 'type' => Type::string()],
        ];
    }

    public function rules()
    {
        return [
            'title' => ['required'],
            'userId' => ['required'],
        ];
    }

    public function resolve($root, $args)
    {
        if(isset($args['userId'])) {
            $args['user_id'] = $args['userId'];
            unset($args['userId']);
        }

        $wish = Wish::create($args);

        if (!$wish) {
            return null;
        }

        if (isset($args['wishlistId'])) {
            $wish->wishlists()->attach($args['wishlistId']);
        }

        return $wish;
    }
}