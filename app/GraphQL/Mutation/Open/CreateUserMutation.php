<?php
namespace App\GraphQL\Mutation\Open;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\User;

class CreateUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'createUser'
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())],
            'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string())],
            'password' => ['name' => 'password', 'type' => Type::nonNull(Type::string())],
            'newsletter' => ['name' => 'newsletter', 'type' => Type::boolean()],
        ];
    }

    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'password' => ['required', 'min:6'],
            'email' => ['required', 'email', 'max:255', 'unique:users']
        ];
    }

    public function resolve($root, $args)
    {
        $args['password'] = bcrypt($args['password']);

        if (isset($args['newsletter']) AND $args['newsletter'] == true) {
            $args['newsletter'] = 1;
        }

        $user = User::create($args);

        if (!$user) {
            return null;
        }

        return $user;
    }
}