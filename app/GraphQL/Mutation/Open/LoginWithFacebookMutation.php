<?php
namespace App\GraphQL\Mutation\Open;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginWithFacebookMutation extends Mutation
{
    protected $attributes = [
        'name' => 'loginWithFacebook'
    ];

    public function type()
    {
        return GraphQL::type('SessionToken');
    }

    public function args()
    {
        return [
            'accesstoken' => ['name' => 'accesstoken', 'type' => Type::nonNull(Type::string())],
        ];
    }

    public function rules()
    {
        return [
            'accesstoken' => ['required'],
        ];
    }

    public function resolve($root, $args)
    {
        $fb = App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
        $token = '';

        $accessToken = $args['accesstoken'];
        $response = 'hello';

        try {
            $response = $fb->get('/me?fields=id,name,email,picture', $accessToken);
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        $userEmail = $response->getGraphUser()->getEmail();
        $userImage = $response->getGraphUser()->getPicture();

        $facebookUserId = $response->getGraphUser()->getId();

        $picture = 'default';

        if (!empty($userImage)) {
            $picture = $userImage['url'];
        }

        $user = $user = User::where(['facebook_user_id' => $facebookUserId])->first();

        if ($user === null) {
            $user = User::where(['email' => $userEmail])->first();

            if($user !== null) {
                $user->update(['facebook_user_id' => $facebookUserId]);
            }
        }

        if($user !== null) {
            $user->update(['picture' => $picture]);

            $token = JWTAuth::fromUser($user);
            JWTAuth::authenticate($token);
        } else {
            $user = User::create(array(
                'email' => $userEmail,
                'name' => $response->getGraphUser()->getName(),
                'password' => 'Registrated with facebook',
                'newsletter' => true,
                'picture' => $picture,
                'facebook_user_id' => $facebookUserId
            ));

            $token = JWTAuth::fromUser($user);
            JWTAuth::authenticate($token);
        }

        return $token;
    }
}