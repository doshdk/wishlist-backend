<?php
namespace App\GraphQL\Mutation\Open;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class GetSessionTokenMutation extends Mutation
{
    protected $attributes = [
        'name' => 'getSessionToken'
    ];

    public function type()
    {
        return GraphQL::type('SessionToken');
    }

    public function args()
    {
        return [
            'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string())],
            'password' => ['name' => 'password', 'type' => Type::nonNull(Type::string())],
        ];
    }

    public function rules()
    {
        return [
            'password' => ['required'],
            'email' => ['required']
        ];
    }

    public function resolve($root, $args)
    {
        $credentials = array(
            'email' => $args['email'],
            'password' => $args['password'],
        );

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return 'invalid_credentials';
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return 'could_not_create_token';
        }

        return $token;
    }
}