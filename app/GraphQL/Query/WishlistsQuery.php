<?php

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Wishlist;

class WishlistsQuery extends Query
{
    protected $attributes = [
        'name' => 'wishlists'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Wishlist'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'userId' => ['name' => 'userId', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return Wishlist::where('id' , $args['id'])->get();
        } elseif (isset($args['userId'])) {
            return Wishlist::where('user_id' , $args['userId'])->get();
        } else {
            return Wishlist::all();
        }
    }
}