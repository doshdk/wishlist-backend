<?php

namespace App\GraphQL\Query\Auth;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Wishlist;
use Tymon\JWTAuth\Facades\JWTAuth;

class MyWishlistsQuery extends Query
{
    protected $attributes = [
        'name' => 'wishlists'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Wishlist'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($user == null) {
            return null;
        }

        if (isset($args['id'])) {
            return Wishlist::where('id' , $args['id'])->get();
        } else {
            return Wishlist::where('user_id' , $user->id)->orderBy('id', 'desc')->get();
        }
    }
}