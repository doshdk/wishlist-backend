<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id', 'cover_image',
    ];


    public function user() {
        return $this->belongsTo('App\User');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function followers() {
        return $this->belongsToMany('App\User', 'wishlist_followers', 'wishlist_id', 'user_id');
    }

    public function wishes() {
        return $this->belongsToMany('App\Wish', 'wishlist_wishes', 'wishlist_id', 'wish_id');
    }
}
