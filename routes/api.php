<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/uploadFile', function(Request $request) {

    $uuid = Uuid::generate();

    $fileName = $uuid . '.' . $request->file('file')->getClientOriginalExtension();

    $request->file('file')->move(base_path() . '/public/uploads/wishes/', $fileName);

    return response()->json([
        'url' => env('APP_URL', '') . '/uploads/wishes/' . $fileName,
    ]);
});
