<?php
ini_set('memory_limit', '-1');
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\User;
use App\Wish;
use App\Wishlist;
use App\Comment;

class UsersTableSeeder extends Seeder
{
    public $wishlists;
    public $users;
    public $faker;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->users = array();
        $this->wishlists = array();

        $this->faker = Faker::create();

        $user = User::where('email', '=', 'info@dosh.dk')->first();

        if($user === null) {
            $user = new User([
                'name' => 'Martin Hobert',
                'email' => 'info@dosh.dk',
                'password' => bcrypt('secret')
            ]);

            $user->save();

            $this->addWishlistsToUser($user);

            array_push($this->users, $user);
        }

        foreach(range(1, 10) as $index) {
            $user = new User([
                'name' => $this->faker->name,
                'email' => $this->faker->unique()->email,
                'password' => bcrypt('secret')
            ]);

            $user->save();

            $this->addWishlistsToUser($user);

            array_push($this->users, $user);
        }

        $this->addFollowersToWishlists();
        $this->addCommentsToWishlists();
    }

    private function addWishlistsToUser(User $user) {

        foreach(range(1, rand(2, 5)) as $index) {
            $wishlist = new Wishlist([
                'title' => $this->faker->realText(40, 2),
                'description' => $this->faker->realText(200, 2),
                'user_id' => $user->id
            ]);

            $wishlist->save();

            array_push($this->wishlists, $wishlist);

            $this->addWishesToWishlist($user, $wishlist);
        }
    }

    private function addWishesToWishlist(User $user, Wishlist $wishlist) {

        foreach(range(1, rand(2, 5)) as $index) {
            $wish = new Wish([
                'title' => $this->faker->realText(40, 2),
                'description' => $this->faker->realText(200, 2),
                'url' => $this->faker->url(),
                'user_id' => $user->id,
            ]);

            $wish->save();

            $wishlist->wishes()->attach($wish);
        }
    }

    private function addFollowersToWishlists() {
        $wishlists = $this->wishlists;

        foreach ($wishlists as $wishlist) {
            $users = User::where('id', '!=', $wishlist->user_id)->inRandomOrder()->limit(rand(2, 10))->get();

            $wishlist->followers()->attach($users);
        }
    }

    private function addCommentsToWishlists() {
        $wishlists = $this->wishlists;

        foreach ($wishlists as $wishlist) {
            $users = User::inRandomOrder()->limit(rand(1, 5))->get();

            foreach ($users as $user) {
                $comment = new Comment([
                    'wishlist_id' => $wishlist->id,
                    'user_id' => $user->id,
                    'message' => $this->faker->realText(200, 2),
                ]);

                $comment->save();

                $this->addRepliesToComment($comment);
            }
        }
    }

    private function addRepliesToComment(Comment $comment) {
        if(rand(1, 10) > 5) {
            $users = User::inRandomOrder()->limit(rand(1, 3))->get();

            foreach ($users as $user) {
                $reply = new Comment([
                    'parent_id' => $comment->id,
                    'user_id' => $user->id,
                    'message' => $this->faker->realText(200, 2),
                ]);

                $reply->save();
            }
        }
    }
}
