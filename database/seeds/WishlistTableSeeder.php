<?php
ini_set('memory_limit', '256M');

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Wishlist;


class WishlistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 40) as $index) {
            $user = User::inRandomOrder()->first();

            $wishlist = new Wishlist([
                'title' => $faker->realText(40, 2),
                'description' => $faker->realText(200, 2),
                'user_id' => $user->id
            ]);

            $wishlist->save();

        }

        $user = User::where('email', '=', 'info@dosh.dk')->first();

        $wishlist = new Wishlist([
            'title' => $faker->realText(40, 2),
            'description' => $faker->realText(200, 2),
            'user_id' => $user->id
        ]);

        $wishlist->save();
    }
}
