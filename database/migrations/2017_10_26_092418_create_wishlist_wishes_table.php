<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishlistWishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wishlist_wishes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wish_id')->unsigned();
            $table->foreign('wish_id')
                ->references('id')
                ->on('wishes')
                ->onDelete('cascade');
            $table->integer('wishlist_id')->unsigned();
            $table->foreign('wishlist_id')
                ->references('id')
                ->on('wishlists')
                ->onDelete('cascade');
            $table->unique(array('wish_id', 'wishlist_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wishlist_wishes', function (Blueprint $table) {
            Schema::dropIfExists('wishlist_wishes');
        });
    }
}
