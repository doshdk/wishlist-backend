<?php


use example\Mutation\ExampleMutation;
use example\Query\ExampleQuery;
use example\Type\ExampleRelationType;
use example\Type\ExampleType;

return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each route
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    // ]
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Rebing\GraphQL\GraphQLController@query',
    //     'mutation' => '\Rebing\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \Rebing\GraphQL\GraphQLController::class . '@query',

    // Any middleware for the graphql route group
    'middleware' => [],

    'graphiql' => [
        'routes' => '/graphiql/{graphql_schema?}',
        'controller' => \Folklore\GraphQL\GraphQLController::class.'@graphiql',
        'middleware' => [],
        'view' => 'graphql::graphiql'
    ],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'default_schema' => 'default',

    // The schemas for query and/or mutation. It expects an array of schemas to provide
    // both the 'query' fields and the 'mutation' fields.
    //
    // You can also provide a middleware that will only apply to the given schema
    //
    // Example:
    //
    //  'schema' => 'default',
    //
    //  'schemas' => [
    //      'default' => [
    //          'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //      ],
    //      'user' => [
    //          'query' => [
    //              'profile' => 'App\GraphQL\Query\ProfileQuery'
    //          ],
    //          'mutation' => [
    //
    //          ],
    //          'middleware' => ['auth'],
    //      ]
    //  ]
    //
    'schemas' => [
        'default' => [
            'query' => [
                'wishlists' => 'App\GraphQL\Query\WishlistsQuery',
            ],
            'mutation' => [
                'getSessionToken' => 'App\GraphQL\Mutation\Open\GetSessionTokenMutation',
                'loginWithFacebook' => 'App\GraphQL\Mutation\Open\LoginWithFacebookMutation',
                'createUser' => 'App\GraphQL\Mutation\Open\CreateUserMutation',
            ],
            'middleware' => []
        ],
        'protected' => [
            'query' => [
                'users' => 'App\GraphQL\Query\UsersQuery',
                'myWishlists' => 'App\GraphQL\Query\Auth\MyWishlistsQuery',
            ],
            'mutation' => [
                'createWishlist' => 'App\GraphQL\Mutation\Auth\CreateWishlistMutation',
                'createWish' => 'App\GraphQL\Mutation\Auth\CreateWishMutation',
                'attachWishesToWishlist' => 'App\GraphQL\Mutation\Auth\AttachWishesToWishlistMutation',
                'detachWishesFromWishlist' => 'App\GraphQL\Mutation\Auth\DetachWishesFromWishlistMutation',
            ],
            'middleware' => ['jwt.autorefresh']
        ]
    ],
    
    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    'types' => [
        'User' => 'App\GraphQL\Type\UserType',
        'Wish' => 'App\GraphQL\Type\WishType',
        'Wishlist' => 'App\GraphQL\Type\WishlistType',
        'Comment' => 'App\GraphQL\Type\CommentType',
        'SessionToken' => 'App\GraphQL\Type\SessionTokenType',
    ],
    
    // This callable will be passed the Error object for each errors GraphQL catch.
    // The method should return an array representing the error.
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    'error_formatter' => [\Folklore\GraphQL\GraphQL::class, 'formatError'],

    // You can set the key, which will be used to retrieve the dynamic variables
    'params_key'    => 'variables',
    
];
